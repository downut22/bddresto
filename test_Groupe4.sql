SET SERVEROUTPUT ON ;
DECLARE
	
BEGIN
	dbms_output.put_line('Procedure afficherReserv( TITOU PIZZA ) ');
	afficherReserv('TITOU PIZZA');
	dbms_output.put_line('Procedure afficherReserv( LA TABLE DE LA MER ) ');
	afficherReserv('LA TABLE DE LA MER');
	dbms_output.put_line('Procedure afficherReserv( FRITES ET MOULES ) ');
	afficherReserv('FRITES ET MOULES');
	dbms_output.put_line('Procedure afficherReserv( RATA TA TOUILLE ) ');
	afficherReserv('RATA TA TOUILLE');
	
	dbms_output.put_line('-                                             -');
	dbms_output.put_line('-                                             -');
	dbms_output.put_line('-                                             -');
	
	dbms_output.put_line('Fonction clientDansResto( 0 , LA TABLE DE LA MER ) ');
	dbms_output.put_line(sys.diutil.bool_to_int(clientDansResto(0,'LA TABLE DE LA MER')));
	dbms_output.put_line('Fonction clientDansResto( 0 , TITOU PIZZA ) ');
	dbms_output.put_line(sys.diutil.bool_to_int(clientDansResto(0,'TITOU PIZZA')));
	
	dbms_output.put_line('-                                             -');
	dbms_output.put_line('-                                             -');
	dbms_output.put_line('-                                             -');
	
	dbms_output.put_line('Trigger reservationsINTO ');
	INSERT INTO RESERVATION VALUES (2,5,'30-12-2020 12:00:00','30-12-2020 13:00:00',2) ; 
	
	dbms_output.put_line('-                                             -');
	dbms_output.put_line('-                                             -');
	dbms_output.put_line('-                                             -');
	
	dbms_output.put_line('Trigger newClient ');
	INSERT INTO PERSONNE VALUES (12,'TESTO','TESTA') ;  
	INSERT INTO CLIENT VALUES (12,'test.test@hotmail.fr') ;  
END;
/
